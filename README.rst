=====
grunt
=====

This formula sets up `grunt <http://gruntjs.com/>`_.

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

Formula Dependencies
====================

* `node formula <https://github.com/saltstack-formulas/node-formula>`_

Available states
================

.. contents::
    :local:

``grunt``
---------

Installs the grunt node package.

``grunt.cli``
-------------

Installs the `grunt cli <http://gruntjs.com/using-the-cli>`_ node package.
