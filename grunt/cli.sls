{% from "grunt/map.jinja" import grunt as grunt_map with context %}

include:
  - node

grunt.cli:
  npm.installed:
    - name: {{ grunt_map['pkg_cli'] }}
    - require:
      - pkg: nodejs
