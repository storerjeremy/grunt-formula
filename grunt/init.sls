{% from "grunt/map.jinja" import grunt as grunt_map with context %}

include:
  - node

grunt:
  npm.installed:
    - name: {{ grunt_map['pkg'] }}
    - require:
      - pkg: nodejs

{% if grunt_map['logdir'] %}
grunt.log:
  file.managed:
    - name: '{{ grunt_map['logdir'] }}/grunt.log'
    - source: ~
    - makedirs: True
    - require_in:
      - npm: grunt

grunt.log.error:
  file.managed:
    - name: '{{ grunt_map['logdir'] }}/grunt.error.log'
    - source: ~
    - makedirs: True
    - require_in:
      - npm: grunt
{% endif %}
